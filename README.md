# oskapps



## What is this?

**OSKApps** is a collection of Microware apps that have been reverse engineered to the original code, or at least as best as possible.

The purpose of this projection include:
- Educational purposes - to learn how a program is made
- Fix bugs - There are some buga in the code, and it would be good to find these and eliminate them
- Add or improve features.

This project will include utilities and perhaps drivers, reverted to their original code - either "C" or assembly.

OSK, as far as I know, is not able to handle git in and of itself.  There is no git port to my knowledge, and very few of us have no direct internet connectivity from OSK.  Therefore, it will be necessary to use an intermediary system to handle git pulls and pushes, and in so doing, the end-of-line character will need to be transposed to display and be workable on OSK.  This can be done in numerous ways, either on the host (the intermediary system) before transferring to OSK, or in OSK afrer trsnsfer.  This will need to be doe if the files are transferred back to the host if git functions need to be used (push, status, log, etc).

To do the end-of-line translation, probably the best app to use would be "tr" if in linux or OSK.  This utility may also be available on Mac or Windows, also.  Uaing this will require sending the translated files to another directory.  One important thing is to be sure to not alter the .git directory, and it would be best to not even send it to OSK.  Some suggestions on ho3w to do the translation include:

On Linux here are a couple options:

- "tr '\n' '\r' < $(find . -type f --exclude=.git) >smme otherdirectory"
- "sed -i /\n/\l/g' $(find . -type f --exclude=.git)"   This command will translate the files in-place without adding a new directory

On OSK, you can do this:
- dir -ud ! grep -v '/$' ! cfp -z "-s=tr '\l' '\n' * >newdir/*"  Note that you would also need to include in the pipe "gr4ep -v '\.git'" if it's still in the structure.

To do this with "tr", you will have to set up the new directory with all subdirs before doing the translation, as "tr" doesn't handle this.

Also, to do the reverse, the "\n"s, '\r"s, and "\l"s will have to be reversed
