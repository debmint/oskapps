#include <stdio.h>
#include <module.h>
#include "fmt.h"

extern mod_exec *modlink();
extern void wrt_root_dir();

int
main(argc,argv)
  register int argc;      /* d4 */
  register char **argv;   /* a2 */
{
    register char *device_name = 0;
    register char *cur_pt;
    register mod_config *modpt;
    register int notused; /* To make code match */

    map_lsn_bgn = 1;

    while (--argc)
    {
        cur_pt = *(++argv);

        if (*cur_pt == '-')
        {
            get_opts(cur_pt);
        }
        else if (device_name)
        {
            exit(_errmsg(1, "only one device name allowed\n"));
        }
        else
        {
            device_name = *argv;
        }
    }

    if (!(device_name))
        exit(_errmsg(1, "no device given\n"));

    puts ("\n                Disk Formatter");

    /* Link to "init" module to get OSK Version of host */

    if ((modpt = (mod_config *)modlink("init", 0, 0)) == (mod_config *)(-1))
    {
        _errmsg(errno, "can't access \"init\" module\n");
    }
    else {
        printf("%s   %s - %d\n", &(((char *)modpt)[modpt->_mos9rev]),
            &(((char *)modpt)[modpt->_minstal]), modpt->_mcputyp); /* L00324 */
        munlink(modpt);
    }

    setup_dskopts(device_name);

    /* Default hard drive to no physical format, then ask */

    if (is_harddrive && !(opt_no_phys_fmt))
    {
        opt_no_phys_fmt = 1;
        fputs("physical format desired?  ", stdout);

        if (input_str(1, "y"))
        {
            opt_no_phys_fmt = 0;
        }
    }

    if ((opt_no_phys_fmt == 0))     /* L00392 */
    {
        opt_tracks += dsk_trk_ofst;
        do_phys_fmt();
    }

    bmapbuf_end = &(bitmap_buf[dsk_sctsiz]);

    if (is_harddrive && !(opt_no_phys_vfy))
    {
        opt_no_phys_vfy = 1;
        fputs("physical verify desired?  ", stdout);

        if (input_str(1, "y"))
        {
            opt_no_phys_vfy = 0;
        }
    }

    /* The entire process is handled in the following  functions */

    wrt_lsn0(sect_buffr);
    verify_dsk();
    wrt_root_dir(sect_buffr);
    wrtintegritycode(sect_buffr);

    if ((close(write_path) == -1))
    {
        exit(_errmsg(errno, "error closing media write path\n"));
    }

    if ((close(read_path) == -1))
    {
        exit(_errmsg(errno, "error closing media read path\n"));
    }

    exit(0);
}
