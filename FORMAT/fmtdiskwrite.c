#include <stdio.h>
#include <rbf.h>
#include <direct.h>
#include <modes.h>
#include "fmt.h"

/* FIXME: We include both a mixture of <direct.h> and <rbf.h>
   There is an overlap, but to get the code to match I had to use
   one at some times and the other elsewhere */

void set_blk_null();
void write_sector();
short creat_dskid();

wrt_lsn0(_ddsect)

/* FIXME: The code matches better if ddsect is defined as
   ddsect *, except for the assignment to dd_versid, which is not present
   in ddsect.   Explore later */

    register struct sector0 *_ddsect;
{
    register int no_no;
    register void *pt_nothing;
    register u_int _uid;
    int have_name = 0;

    set_blk_null(_ddsect);
    _uid = getuid();
    int_to_byt3(dsk_tot_sect, &(_ddsect->dd_tot[0]));
    _ddsect->dd_tks = (char)dsk_spt;
    _ddsect->dd_map = (((dsk_tot_sect/opt_clustrsiz) + 7) / 8);  /* $02414 */
    _ddsect->dd_bit = opt_clustrsiz;
    root_lsn = ((_ddsect->dd_map + trk0_free) >> sct_bitpos) + 1;
    int_to_byt3(root_lsn, &(_ddsect->dd_dir[0]));
    _ddsect->dd_own[1] = _uid;
    _ddsect->dd_own[0] = _uid >> 16;
    (char)_ddsect->dd_att |= -1;    /* cast to make code match */

    if (!(is_harddrive))
    {
        _ddsect->dd_fmt = (((char)dsk_trkDns + (char)fmt_dbldns)) + ((char)opt_sides - 1);  /* b @ 16 */
    }

    int_to_short(dsk_spt, _ddsect->dd_spt);        /* L02484 */
    getime(_ddsect->dd_date);
    dskid_seed = ((char)(_ddsect->dd_name)[0]);

    if ((opt_VolName == 0))
    {
        do {
            fputs("volume name:  ", stdout);    /* L024ac */
            input_str(0);

            if ((strlen(inkeybuf) > 33))
            {
                fprintf(stdout, "volume name to long ( 32 characters maximum ).\n");
            }
            else
            {
                ++have_name;      /* L024e4 */
                opt_VolName = inkeybuf;
                inkeybuf[strlen(inkeybuf) - 1] = '\0';
            }
        } while(have_name == 0);
    }

    strncpy(_ddsect->dd_name, opt_VolName, sizeof(_ddsect->dd_name));       /* L02506 */
    _ddsect->dd_name[strlen(_ddsect->dd_name) - 1] |= 0x80;
    _ddsect->dd_dsk = creat_dskid(_ddsect->dd_date);
    _strass(_ddsect->dd_opt, &dsk_opt, 32);
    _ddsect->dd_maplsn = map_lsn_bgn;
    _ddsect->dd_lsnsize = (u_short)dsk_sctsiz;
    _ddsect->dd_versid = 1;
    write_sector(_ddsect);
}

void
set_blk_null(a2)
    /* a2 is really a struct ddsect * */
    register int *a2;
{
    register char *a3 = &(((char *)a2)[dsk_sctsiz]);

    while (a2 < a3)
    {
        *(a2++) = 0;
    }
}

void
write_sector(srcptr)      /* L02598 */
    register void *srcptr;
{
    if ((dsk_sctsiz != write(write_path, srcptr, dsk_sctsiz)))
    {
        exit(_errmsg(errno, "FATAL error writing system sector\n"));
    }
}

struct dot_dot {
    struct direntry dotdot;
    struct direntry dot;
};

/* FIXME: It would be more appropriate to define fdblk as
   struct fd, from rbf, but fd_fsize is defined as u_int there,
   and the code sets it in terms of char[].
   To change to rbf.h, would have to set "fdblk-?fd_fsize=64" and
   redo fd_own/ */
void
wrt_root_dir(fdblk)      /* L025e0 */
    register struct fildes *fdblk;
{
    register struct dot_dot *dots = (struct dot_dot *)fdblk;
    register short sectcount;

    puts("writing root directory structure");
    set_blk_null(fdblk);
    fdblk->fd_att = (S_IPRM ^ S_ISHARE);     /* 191 */
    fdblk->fd_own[0] = 0;
    fdblk->fd_own[1] = 1;
    getime((fdblk->fd_date));
    fdblk->fd_link = 2;
    fdblk->fd_fsize[0] = 0;
    fdblk->fd_fsize[1] = 0;
    fdblk->fd_fsize[2] = 0;
    fdblk->fd_fsize[3] = 64;

    /* Set date-created same as date-modified */

    fdblk->fd_dcr[0] = fdblk->fd_date[0];
    fdblk->fd_dcr[1] = fdblk->fd_date[1];
    fdblk->fd_dcr[2] = fdblk->fd_date[2];

    /* Initialize the first fdseg */

    int_to_byt3((root_lsn + 1), (fdblk->fdseg[0].addr));
    int_to_short(dsk_sas, (fdblk->fdseg[0].size));

    write_sector(fdblk);

    /* Insert "." and ".." into dirent for root */

    set_blk_null(dots);
    dots->dotdot.dir_name[0] = '.';
    dots->dotdot.dir_name[1] = ('.' | 0x80);
    dots->dotdot.dir_addr = root_lsn;
    dots->dot.dir_name[0] = ('.' | 0x80);
    dots->dot.dir_addr = root_lsn;
    write_sector(dots);
    set_blk_null(dots);
    sectcount = dsk_sas - 1;

    /* Fill remainder of segment with nulls */

    while ((sectcount > 0))
    {
        write_sector(dots);
        --sectcount;
    }
}

short
creat_dskid(a2)
    register short *a2;
{
    register short d_id = dskid_seed;
    register int didntuse;
    register short count = 20;

    while (count)
    {
        d_id += (*a2 + 62);
        --count;
        ++a2;
    }

    return d_id;
}

/* Set up this fake ddsect in order to set dd_sync as an int */

struct fakes0 {
   char fake_fill[95];
   u_int fake_sync;
};

wrtintegritycode(sectdata)    /* L026f0 */
    register void *sectdata;
{
    if ((lseek(write_path, 0, SEEK_SET) == -1) || (lseek(read_path, 0, SEEK_SET) == -1) )
    {
        exit(_errmsg(errno, "can't reposition to sector zero\n"));
    }

    if((read(read_path, sectdata, dsk_sctsiz) != dsk_sctsiz))
    {
        exit(_errmsg(errno, "can't read back sector zero\n"));
    }

    ((struct fakes0 *)sectdata)->fake_sync = 0x4372757a;   /* 4372757a */

    if ((write(write_path, sectdata, dsk_sctsiz) == -1))
    {
        exit(_errmsg(errno, "can't write media integrity code\n"));
    }
}
