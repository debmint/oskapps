
/* This is just a guess to make the code work */
struct disk_headers {
    short padCount;
    short padVal;
};

#include "fmt.h"

struct disk_headers *D001ae;

struct disk_headers tbl_SD[] = {      /* D0054c */

        /* HEADER (once per track) */

    { 1, 0x0000}, {40, 0xff00}, {6, 0x0000},
    { 1, 0xfc00}, {12, 0xff00}, {0, 0x0000},

        /* SECTOR HEADER (each sector) */

    { 6, 0x0000}, { 1, 0xfe00}, {4, 0x0100},
    { 1, 0xf700}, {10, 0xff00}, {6, 0x0000},
    { 1, 0xfb00}, { 0, 0xe500},

        /* Follows DATA */

    {1, 0xf700},  {10, 0xff00}, { 0, 0x0000},
  
    /* Fill character for remainder of sector */
    {0, 0xff00},

    {67, 0x0000}
};

struct disk_headers tbl_DD[] = {      /* D00598 */

        /* HEADER (once per track) */

    {80, 0x4e00}, {12, 0x0000}, { 3, 0xf600},
    { 1, 0xfc00}, {32, 0x4e00}, { 0, 0x0000},

        /* SECTOR HEADER (each sector) */

    {12, 0x0000}, { 3, 0xf500}, { 1, 0xfe00},
    { 4, 0x0100}, { 1, 0xf700}, {22, 0x4e00},
    {12, 0x0000}, { 3, 0xf500}, { 1, 0xfb00},
    { 0, 0xe500},

        /* Follows DATA */

    { 1, 0xf700}, {22, 0x4e00}, { 0, 0x0000},

    /* Fill character for remainder of sector */
    { 0, 0x4e00},

    {144, 0x0000}
};
int trk0_written = -1;
int current_track = 0;    /* D005f0 */
int current_side = 0;     /* D005f4 */
int current_spt = 0;      /* D005f8 */


void init_sectors();

void write_interleave();
struct disk_headers *write_tbl_block();

do_phys_fmt()      /* L015b6 */
{
    register int _siden;
    register int _trackn;
    register int _curSide;
    register int _dnsity = 0;

    allocate_trkbuf();

    if ((_ss_rest(write_path) < 0))
    {
        exit(_errmsg(errno, "error restoring to track zero\n"));
    }

    if (is_ddtrk0)    /* L015ee */
    {
        _dnsity = fmt_dbldns;
    }

    if (can_autosiz)    /* L015fa else L0167a */
    {
        _trackn = dsk_trk_ofst;

        _curSide = 0;
        _dnsity = fmt_dbldns;
        init_sectors(_trackn, _curSide, _dnsity);
        _siden = ((((dsk_trkDns + _dnsity) & 0xff) + ((_curSide << 8) & 0xffff)));

        if ( !(is_harddrive) )
        {
            ++_siden;
        }

        if ((_ss_wtrk(write_path, _trackn, _siden, opt_ilv, trkbuf, ilvptr) < 0))    /* L0163e */
        {
            exit(_errmsg(errno, "error formatting entire media\n"));
        }
    }
    else
    {
        _trackn = dsk_trk_ofst;      /* L0167a */

        while ((_trackn < opt_tracks)) 
        {
            _curSide = 0;

            while (_curSide < opt_sides)
            {
                init_sectors(_trackn, _curSide, _dnsity);
                _siden = ((((dsk_trkDns + _dnsity) & 0xff) + ((_curSide << 8) & 0xffff)));

                if ( !(is_harddrive) && (_curSide))
                {
                    ++_siden;
                }

               if ((_ss_wtrk(write_path, _trackn, _siden, opt_ilv, trkbuf, ilvptr) < 0))    /* L0163e */
               {
                   exit(_errmsg(errno, "error writing track: cyl $%03x, head $%02d\n", _trackn, _curSide));
               }

               _dnsity = fmt_dbldns;      /* L016fa */
               ++_curSide;
            }

            ++_trackn;
        }
    }
}

/* Determine track size in bytes and allocate buffer for it */

allocate_trkbuf()      /* L01726 */
{
    register struct disk_headers *ia_ptr;
    register int  _write_count;

    if ((is_harddrive || can_autosiz))
    {
        trk_bytes = dsk_sctsiz;      /* L01744 */
    }
    else {
        ia_ptr = (fmt_dbldns ? tbl_DD : tbl_SD);
        trk_bytes = 0;

        /* Get Track Header */

        while (_write_count = (ia_ptr++)->padCount)
        {
            trk_bytes += _write_count;    /* L01764 */
        }

        /* Get Sector Header */

        trkbufSkip = 0;

        while (_write_count = (ia_ptr++)->padCount)
        {
            trkbufSkip += _write_count;    /* L01776 */
        }

        trkbufSkip += dsk_sctsiz;

        /* Read block 3 of table */

        while (_write_count = (ia_ptr++)->padCount)
        {
            trkbufSkip += _write_count;    /* L0178c */
        }

        /* Sector complete.. jump to next */

        trk_bytes += (trkbufSkip * dsk_spt);
        trk_bytes += 1024;
    }

    if (!(trkbuf = (char *)malloc(trk_bytes)))    /* L017b2 */
    {
        exit(_errmsg(errno, "can't get track buffer (%d bytes)\n", trk_bytes));
    }
}

void
init_sectors(trkno, _side, _density)
    int trkno;
    int _side;
    int _density;
{
    int _is_sect0;
    register struct disk_headers *tbl_blk_begin = tbl_SD;
    register struct disk_headers *_cur_tbl_blk;
    register char *wrt_ptr;
    register int _sect_count;
    register int _my_spt;
    register char *fill_chr;
    register struct disk_headers *tmp_ptr;

    curTrkbufPos = trkbuf;

    if (is_harddrive || can_autosiz)    /* else L0184a */
    {
        if (trk0_written == _density)    /* L0180c else L0198a */
        {
            return;
        }

        /* Fill allocation sector with 0xe5's */

        _sect_count = 0;
        wrt_ptr = curTrkbufPos;

        while (dsk_sctsiz > _sect_count)
        {
            *(wrt_ptr++) = 0xe5;    /* L01820 */
            ++_sect_count;
        }

        write_interleave(dsk_spt, trkno, _side);
        trk0_written = _density;
        return;
    }

    /* The following is for floppies */

    _sect_count = dsk_spt_0;    /* L0184a */

    if (trk0_written != _density)    /* else L0190c */
    {
        _is_sect0 = 1;

        if (_density)
        {
            tbl_blk_begin = tbl_DD;
            /* Here, we use _sect_count as sectors/track */
            _sect_count = dsk_spt;
        }

        trk0_written = _density;    /* L01870 */

            /* Write Track Header */

        tbl_blk_begin = write_tbl_block(tbl_blk_begin);
        ++tbl_blk_begin;      /* Point to begin of Block 2 */

        do {
            if (_is_sect0)    /* L01880 */
            {
                trkbufSkip = 0;
                tmp_ptr = tbl_blk_begin;

                while ((_my_spt = (tmp_ptr++)->padCount))
                {
                    trkbufSkip += _my_spt;    /* L0188c */
                }
            }

                /* Write Sector Header */

            _cur_tbl_blk = write_tbl_block(tbl_blk_begin);     /* L0189a */
            ++_cur_tbl_blk;    /* Point to begin of Block 3 */

            if (_is_sect0)     /* else L018c4 */
            {
                trkbufSkip += dsk_sctsiz;
                tmp_ptr = _cur_tbl_blk;

                while ((_my_spt = ((tmp_ptr++)->padCount)))
                {
                    trkbufSkip += _my_spt;    /* L018b4 */
                }

                _is_sect0 = 0;
            }

            _my_spt = 0;    /* L018c4 */

                /* Fill sector DATA block */

            while ((dsk_sctsiz > _my_spt)) {
                *(curTrkbufPos++) = 0xe5;    /* L018c8 */
                ++_my_spt;
            }

            /* Write SECTOR Trailer */
            _cur_tbl_blk = write_tbl_block(_cur_tbl_blk);
        } while((--_sect_count));    /* @018e4 */

        ++_cur_tbl_blk;    /* Point to Block 4 */
        fill_chr = (char *)(&(_cur_tbl_blk->padVal));
        wrt_ptr = curTrkbufPos;

        /* Write track Trailer */

        while (wrt_ptr < &(trkbuf[trk_bytes]))
        {
            *(wrt_ptr++) = *(fill_chr);     /* L018f6 */
        }

        D001ae = _cur_tbl_blk;
    }
    else {
        _cur_tbl_blk = D001ae;    /* L0190c */
    }

    if (_density)       /* L1910 */
    {
        _my_spt = dsk_spt;
    }
    else
    {
        _my_spt = dsk_spt_0;
    }

    write_interleave(_my_spt, trkno, _side);
    ++_cur_tbl_blk;
    wrt_ptr = &(trkbuf[_cur_tbl_blk->padCount]);
    _sect_count = 0;

    while (_sect_count < _my_spt)
    {
        wrt_ptr[0] = (char)trkno;
        wrt_ptr[1] = (char)_side;
        wrt_ptr[2] = ilvptr[_sect_count] + (char)dsk_sect_ofst;

        switch (dsk_sctsiz)
        {
            case 256:    /* L0195e */
                wrt_ptr[3] = 1;
                break;
            case 512:    /* L01966 */
                wrt_ptr[3] = 2;
                break;
            case 1024:   /* L0196e */
                wrt_ptr[3] = 3;
                break;
            default:     /* L01976 */
                wrt_ptr[3] = 0;
                break;
        }

        ++_sect_count;
        wrt_ptr += trkbufSkip;
    }
}

void
write_interleave(_spt, cur_trkno, cur_side)       /* L019b4 */
    register int _spt;
    int cur_trkno;
    int cur_side;
{
    register char *_ilvptr_1 = ilvptr;
    register char *_ilvptr_2 = ilvptr;
    register int d5;

    if (cur_trkno != current_track){
        if ((current_spt += 2) >= _spt)
        {
            current_spt -= _spt;
        }

        current_track = cur_trkno;
    }
    else if (cur_side != current_side)      /* L019f0 */
    {
        if (++current_spt >= _spt)
        {
            current_spt -= _spt;
        }

        current_side = cur_side;
    }

    d5 = 0;    /* L01a0e */

    while (d5 < _spt)
    {
        *_ilvptr_1 = d5;     /* L01a12 */
        _ilvptr_1 += opt_ilv;
        if ((_ilvptr_1 >= (&ilvptr[_spt])) && ((_ilvptr_1 -= _spt) == _ilvptr_2))
        {
            ++_ilvptr_1;
            _ilvptr_2 = _ilvptr_1;
        }

        ++d5;
    }
}

struct disk_headers *
write_tbl_block(tblptr)
    register struct disk_headers *tblptr;
{
    register char *_bufptr = curTrkbufPos;
    register char *val_ptr;
    register int counter;

    while (counter = tblptr->padCount)
    {
        val_ptr = (char *)(&(tblptr->padVal));

        do
        {
            *(_bufptr++) = *val_ptr;
        } while (--counter);

        ++tblptr;
    }

    curTrkbufPos = _bufptr;
    return tblptr;
}
