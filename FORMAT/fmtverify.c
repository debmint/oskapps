#include <stdio.h>
#include <setsys.h>
#include "fmt.h"

char *vfy_buf;   /* D001b2 */
int clustersiz;  /* D001b6 */
char *bitmapbuf_ptr;    /* D001ba */
short bitPos;    /* D001be */

void setBitmapBit();

verify_dsk()      /* L01b0c */
{
    int raw_bgn_free;
    int verifiedsects;
    u_int bgn_free;
    int free_cluster_01;
    int fastvfy_ok;
    int use_fast_clstrsiz;

    readpth_open();
    read(read_path, sect_buffr, dsk_sctsiz);
    close (read_path);
    readpth_open();

    /* Fast verify only if cluster size < 32 */

    fastvfy_ok = (fast_clstrbytes > (clustersiz = (dsk_sctsiz * opt_clustrsiz)));

    /* Allocate 32-sector buffer if fast verify is in effect */

    if (fastvfy_ok)
    {
        if ( !(vfy_buf = (char *)malloc(fast_clstrbytes)) )
        {
            fastvfy_ok = 0;
            errno = 0;
        }
    }

    /* If not doing fast verify, allocate buffer of size of actual cluter */

    if ( !(fastvfy_ok) )          /* L01b86 */
    {
        if ( !(vfy_buf = ((char *)malloc(clustersiz))) )
        {
            exit(_errmsg(errno, "can't get verify buffer (%d bytes)\n", clustersiz));
        }
    }

    use_fast_clstrsiz = ((opt_no_fast_vfy == 0) && (fastvfy_ok != 0));    /* L01bb2 */

    /* first free sector plus default sector allocation size */
    raw_bgn_free = ((root_lsn + 1) + (dsk_sas));

    /* odd sectors from above past opt_clustersiz boundary */
    if ((bgn_free = (raw_bgn_free % opt_clustrsiz)))
    {
        /* This should point to cluster 1 - "remainder" */
        bgn_free = opt_clustrsiz - bgn_free;

        /* I _think_ this points to the first sector of the first
           available cluster */

        raw_bgn_free += bgn_free;
        dsk_sas += bgn_free;       /* Is this ever used? */
    }

    verifiedsects = 1;    /* L01c08 */

    /* Convert sector to cluster */

    free_cluster_01 = raw_bgn_free/opt_clustrsiz;

    if ((opt_no_phys_vfy == 0))
    {
        printf("verifying media, ");
    }

    printf("building media bitmap...\n");    /* L01c32 */

    if (can_autosiz)
    {
        verifiedsects = mkbitmap_autosz(verifiedsects, free_cluster_01, use_fast_clstrsiz);
    }
    else {    /* L01c58 */
        verifiedsects = mkbitmap_noautosz(verifiedsects, free_cluster_01, use_fast_clstrsiz);
        --verifiedsects;
    }

    unusblsects = verifiedsects % opt_clustrsiz;     /* L01c70 */

    while ((sect_written == 0))
    {
        setBitmapBit(1);    /* L01c86 */
    }

    printf("\n    quantity good $%08lx  %10ld  (%10ld bytes)\n",
                    goodsects, goodsects, (goodsects * dsk_sctsiz));
    printf("     quantity bad $%08lx  %10ld  (%10ld bytes)\n",
                    badsects, badsects, (badsects * dsk_sctsiz));
    printf("quantity unusable $%08lx  %10ld  (%10ld bytes)\n",
                    unusblsects, unusblsects, (unusblsects * dsk_sctsiz));
    printf(" sectors verified $%08lx  %10ld  (%10ld bytes)\n",
                    verifiedsects, verifiedsects, (verifiedsects * dsk_sctsiz));
}

readpth_open()
{
    if (((read_path = open(dvc_nam, 1)) == -1))
    {
        exit(_errmsg(errno, "can't open '%s' for reading\n", dvc_nam));
    }
}

/* Use this if the device can report size */

int
mkbitmap_autosz(goodsects, free_clustr, do_fastvfy)    /* L01d72 */
    int goodsects;
    int free_clustr;
    register int do_fastvfy;
{
    register int cur_sect;
    register int clstr_cnt;
    register int fastclstr_cnt;

    int _clstr_sz;
    int _trksects;
    int sectnum;

    fastclstr_cnt = 0;
    _clstr_sz = ((opt_clustrsiz > dsk_spt) ? opt_clustrsiz : dsk_spt);
    clstr_cnt = 0;

    if (opt_no_phys_vfy)
    {
        do_fastvfy = 0;
    }

    _trksects = ((is_harddrive) ? 8 : 16);    /* L01dac */
    sectnum = 0;
    cur_sect = 0;

    while ((totsects > cur_sect))
    {
        if ((opt_showvfytime) && (cur_sect % _trksects == 0))     /* L01dc4 */
        {
             tik_tot = _getsys(D_Ticks, 4);
        }

        if ((clstr_cnt == 0)
            && do_fastvfy
            && (free_clustr == 0)
            && (++fastclstr_cnt >= 0)
            && ((clstr_cnt = read_fastclstr()) == 0)
            )      /* L01de2 */
        {
            fastclstr_cnt = -32;
        }

        if (((cur_sect % opt_clustrsiz) == 0))    /* L01dfe */
        {
            if (free_clustr)
            {
                --free_clustr;

                if ((verify_cluster(1, clstr_cnt)) == 0)
                {
                    exit(_errmsg(1, "FATAL error - bad system sector\n"));
                }
            }
            else {
                verify_cluster(0, clstr_cnt);      /* L01e36 */
            }
        }

        if (do_fastvfy && clstr_cnt)     /* L01e3e */
        {
            --clstr_cnt;
        }

        if ((opt_no_phys_vfy == 0))      /* L01e48  else L01ee4 */
        {
            if ((cur_sect % _clstr_sz) == 0)
            {
                printf((is_harddrive ? "%06x" : "%03x"), cur_sect);
                fflush(stdout);
                ++sectnum;

                if((_trksects != sectnum))
                {
                    putc(' ', stdout);
                }
                else
                {
                    if (opt_showvfytime)     /* L01e98 */
                    {
                        tik_tot = _getsys(D_Ticks, 4) - tik_tot;
                        printf( " (%d.%02d)", (tik_tot/Tck_Sec), (tik_tot % Tck_Sec));
                    }

                    putc('\n', stdout);
                    sectnum = 0;
                }
            }
        }    /* L01ee4 */

        ++cur_sect;
    }

    return totsects;
}

/* Use this if the device cannot report size */

mkbitmap_noautosz(goodsects, free_clustr, do_fastvfy)      /* L01f00 */
    int goodsects;
    int free_clustr;
    int do_fastvfy;
{
    int fastclstr_cnt;
    int _trkscts;
    register int cur_trk;
    register int max_trk;
    register int _my_spt;
    register int clstr_cnt;

    fastclstr_cnt = 0;

    _my_spt = (dsk_trk_ofst ? dsk_spt : dsk_spt_0);
    _trkscts = 16;
    clstr_cnt = 0;

    if (opt_no_phys_vfy)
    {
        do_fastvfy = 0;
    }

    max_trk = dsk_tot_trks;        /* L01f3c */
    cur_trk = 0;

    while (cur_trk < max_trk)
    {
        if ((opt_showvfytime) && ((cur_trk % _trkscts) == 0))     /* L01f46 */
        {
            tik_tot = _getsys(D_Ticks, 4);
        }

        while ((_my_spt--))
        {
            if ((clstr_cnt == 0)
                    && do_fastvfy
                    && (free_clustr == 0)
                    && ((++fastclstr_cnt) >= 0)
                    && ((clstr_cnt = read_fastclstr()) == 0) )
            {
                fastclstr_cnt = -32;
            }

            if ((goodsects % opt_clustrsiz) == 0)   /* else L01fd0 */
            {
                if (free_clustr)
                {
                    --free_clustr;

                    if (verify_cluster(1, clstr_cnt) == 0)
                    {
                        exit(_errmsg(1, "FATAL error - bad system sector\n"));
                    }
                }
                else
                {
                    verify_cluster(0, clstr_cnt); /* L01fc8 */
                }
            }

            ++goodsects;              /* L01fd0 */

            if ((do_fastvfy) && (clstr_cnt > 0))
            {
                --clstr_cnt;
            }
        }        /* L01fe0 */

        if ((opt_no_phys_vfy == 0))
        {
            printf("%03x", cur_trk);
            fflush(stdout);

            if ((cur_trk + 1) % _trkscts)
            {
                putc(' ', stdout);
            }
            else
            {
                if (opt_showvfytime)
                {
                    tik_tot = _getsys(D_Ticks, 4) - tik_tot;
                    printf(" (%d.%02d)", tik_tot/Tck_Sec, tik_tot % Tck_Sec);
                }

                putc('\n', stdout);
            }
        }      /* L02066 */

        _my_spt = dsk_spt;      /* L02066 */
        ++cur_trk;
    }

    return goodsects;
}

read_fastclstr()        /* L02084 */
{
    long cur_lsn;

    cur_lsn = lseek(read_path, 0, SEEK_CUR);

    if (fast_clstrbytes != read(read_path, vfy_buf, fast_clstrbytes))
    {
        lseek(read_path, cur_lsn, SEEK_SET);
        return 0;
    }

    return 32;
}

verify_cluster(clstr_is_used, try_autosiz)      /* L020ea */
    register int clstr_is_used;
    int try_autosiz;
{
    register int read_good = 1;

    errno = 0;

    if ((opt_no_phys_vfy) || (try_autosiz))
    {
        if (can_autosiz)     /* L02110 else L02154 */
        {
            read_good = ((totsects - (goodsects + badsects)) >= opt_clustrsiz);
        }
    }
    else
    {
        read_good = (clustersiz == read(read_path, vfy_buf, clustersiz));
    }

    if (read_good)     /* L02154 */
    {
        goodsects += opt_clustrsiz;
        setBitmapBit(clstr_is_used != 0);
        return read_good;
    }

    if (errno)          /* L0216a */
    {
        badsects += opt_clustrsiz;
        fput_line("\n");
        prerr(0, errno);
    }
    else
    {
        ++curnt_vfy_cluster;       /* L02190 */
    }

    lseek(read_path, (((opt_clustrsiz * curnt_vfy_cluster) + (goodsects + badsects)) * dsk_sctsiz), SEEK_SET);
    setBitmapBit(1);
    return read_good;
}

void
setBitmapBit(isUsed)          /* L021d4 */
    register int isUsed;
{
    /* Initialize bitmapbuf_ptr if not already */

    if ((bitmapbuf_ptr == 0))
    {
        bitmapbuf_ptr = bitmap_buf;
        bitPos = 8;
        sect_written = 0;
    }

    /* Set the bit, which os done by shifting the byte left and adding the value */

    *bitmapbuf_ptr = (((*bitmapbuf_ptr) << 1) + isUsed);

    if (((--bitPos) == 0))
    {
        ++bitmapbuf_ptr;

        if ((bmapbuf_end <= bitmapbuf_ptr))
        {
            write_sector(bitmap_buf);
            bitmapbuf_ptr = 0;
            sect_written = 1;
        }
        else
        {
            bitPos = 8;
        }
    }
}
