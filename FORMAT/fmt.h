#include <sgstat.h>
#include <errno.h>
#include <types.h>

#ifdef MAIN
#   define xt
#else
#define xt extern
#endif

#ifndef SEEK_SET
#    define SEEK_SET 0
#    define SEEK_CUR 1
#endif

xt u_int dsk_tot_sect;    /* D0001e */
xt int dsk_tot_trks;      /* D00022 */
xt u_int dsk_sctsiz;      /* D00028 */
xt u_int trk0_free;       /* D0002a */ /* Track 0 sectors less Identification sector */
xt u_int sct_bitpos;      /* D0002e */
xt int maxclusters;       /* D00032 */
xt u_int root_lsn;        /* D00036 */
xt short is_harddrive;    /* D0003a */
xt short dvc_siz;         /* D0003c */
xt short fmt_dbldns;      /* D0003e */
xt short dsk_trkDns;      /* D00040 */
xt short dsk_spt_0;       /* D00042 */
xt short dsk_spt;         /* D00044 */
xt short dsk_sas;         /* D00046 */
xt short write_path;      /* D00048 */
xt short read_path;       /* D0004a */
xt short opt_clustrsiz;   /* D0004c */
xt short opt_no_phys_fmt; /* D0004e */
xt short opt_no_phys_vfy; /* D00050 */
xt short opt_no_fast_vfy; /* D00052 */
xt short opt_ready;       /* D00054 */
xt short is_ddtrk0;       /* D00056 */
xt short opt_dnsity;      /* D00058 */
xt short opt_tracks;      /* D0005a */
xt short opt_ilv;         /* D0005c */
xt short opt_sides;       /* D0005e */
xt short dsk_trk_ofst;    /* D00060 */
xt short dsk_sect_ofst;   /* D00062 */
xt short dvc_typ;         /* D00064 */
xt short min_sas;         /* D00066 */
xt struct _sgr dsk_opt;   /* D00068 */
xt int badsects;          /* D000e8 */
xt int goodsects;         /* D000ec */
xt int unusblsects;       /* D000f0 */
xt int curnt_vfy_cluster; /* D000f4 */
xt int neverused_00f8;    /* D000f8 */
xt int trk_bytes;         /* D000fc */
xt int fast_clstrbytes; /* D00100 */
xt u_int ilvtbl_siz;    /* D00104 */
xt int trkbufSkip;          /* D00108 */
xt int tmpnum;          /* D0010c */
xt short sect_written;  /* D00110 */
xt short dskid_seed;    /* D00112 */
xt int can_autosiz;     /* D00114 */
xt int totsects;        /* D00118 */

/* The following is a kludge, it seems that totsects is a single int
   and there's no reference to the nxt 8 bytes */

xt int data_notused[2];
xt int opt_showvfytime; /* D00124 */
xt u_int Tck_Sec;       /* D00128 */
xt int is_floppy;       /* D0012c */
xt int is_removable;    /* D00130 */
xt int map_lsn_bgn;     /* D00134 */
xt u_int tik_tot;       /* D00138 */
xt char inkeybuf[64];   /* D0013c */
xt char dvc_nam[22];    /* D0017c */
xt char *ilvptr;        /* D00192 */
xt int sect_buffr;      /* D00196 */
xt char *bitmap_buf;    /* D0019a */
xt char *opt_VolName;   /* D0019e */
xt char *trkbuf;        /* D001a2 */
xt char *curTrkbufPos;  /* D001a6 */
xt char *bmapbuf_end;   /* D001aa */
