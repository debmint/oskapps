#define MAIN /* put it here because this uses the most data variables */

#include <stdio.h>
#include <errno.h>
#include <modes.h>
#include <ctype.h>
#include <rbf.h>
#include "fmt.h"

int D00548 = -1;

#define min(a,b) a > b ? b : a

void set_dsk_format();
void report_dsk();
void set_floppy_fmt();

extern int get_totsect();

setup_dskopts(devnm)
    char *devnm;
{
    int need_prompt = 0;
    int minclstrsiz;
    int min_spt;

    /* Set up pathname for device */

    if ((devnm[0] != '/'))
    {
        dvc_nam[0] = '/';
        dvc_nam[1] = '\0';
    }
    else {
        dvc_nam[0] = '\0';
    }

    strcat(dvc_nam, devnm);     /* L00582 */
    strcat(dvc_nam, "@");

    if (((write_path = open(dvc_nam, S_IWRITE)) == -1))
    {
        showhelp();
        exit(_errmsg(errno, "can't open '%s' for writing\n", devnm));
    }

    get_path_opts();    /* L005d2 */

    /* Loop to get keyboard input to set up disk format, and get go-ahead
       to format if prompting has been specified */

    do {
        set_dsk_format();
        min_spt = ((dsk_spt_0 <= dsk_spt) ? dsk_spt_0 : dsk_spt);

        if (min_spt && opt_ilv)
        {
            if( opt_ilv > min_spt - 1)
            {
                _exit(_errmsg(1, "Interleave factor may not exceed %d.\n", min_spt));
            }
        }
        else if (opt_ilv == 0)     /* L0061a */
        {
            opt_ilv = 1;
        }

        report_dsk();    /* L00626 */
        printf("Formatting device:  %s\n", devnm);

        if (opt_ready == 0)      /* ELSE L006f2 */
        {
            if (need_prompt)
            {
                puts("y(es), n(o), or q(uit)");
                need_prompt = 0;
            }

            fputs("proceed?  ", stdout);     /* L00654 */
            input_str(0);

            switch (_tolower(*inkeybuf))
            {
                case 'q':    /* L0066c */
                    exit(0);
                    break;   /* Needless?  Could save 4 bytes... */
                case 'y':    /* L00676 */
                    if (is_harddrive)
                    {
                        fputs("this is a HARD disk - are you sure? ", stdout);

                        if ((input_str(1, "y") == 0))
                        {
                            exit(0);
                        }
                    }

                    opt_ready = 1;      /* L006a2 */
                    continue;
                case 'n':    /* L006aa */
                    if ((is_harddrive) || (can_autosiz))
                    {
                        exit(0);
                    }

                    set_floppy_fmt();    /* L006bc */
                    continue;
                default:
                    need_prompt = 1;
                    continue;
            }
        }
    } while (opt_ready == 0);    /* @ L006f2 */

    /* Add this value to dd_map (bytes in allocation map) to make sure
       there is enough space in the allocation map when setting the
       start of first (root) directory. */
       
    trk0_free = dsk_sctsiz - 1;

    /* Save the bit position in sector size.  Sector size is always
       a power of two */

    sct_bitpos = 0;

    while (((1 << sct_bitpos) & dsk_sctsiz) == 0)
    {
        ++sct_bitpos;    /* L0070e */
    }

    maxclusters = 65535 * 8;
    fast_clstrbytes = dsk_sctsiz * 32;

    if (can_autosiz)
    {
        dsk_tot_trks = 1;
        dsk_tot_sect = totsects;
        ilvtbl_siz = 32;
    }
    else {
        dsk_tot_trks = opt_tracks * opt_sides;      /* L00744 */
        dsk_tot_sect = (dsk_tot_trks - 1) * dsk_spt;
        dsk_tot_sect += dsk_trk_ofst ? dsk_spt : dsk_spt_0;        /* L00774 */
        ilvtbl_siz = (dsk_spt > dsk_spt_0) ? dsk_spt : dsk_spt_0;  /* L0078a */

    }

    neverused_00f8 = dsk_tot_sect * dsk_sctsiz;

    if (maxclusters < dsk_tot_sect/opt_clustrsiz)    /* else L00832 */
    {
        minclstrsiz = (dsk_tot_sect + maxclusters - 1)/maxclusters;

        if (((minclstrsiz - 1) & minclstrsiz) != 0)
        {
            while ((min_spt = (minclstrsiz - 1) & minclstrsiz))
            {
                minclstrsiz = min_spt;      /* L007da */
            }

            minclstrsiz *= 2;
        }

        fprintf(stderr, "\nformat: error - ");
        fprintf(stderr, "cluster size is too small for this device.\n");
        fprintf(stderr, "cluster size must be at least %d.\n", minclstrsiz);
        exit(1);
    }

    if (((sect_buffr = malloc(dsk_sctsiz)) == 0))    /* L00832 */
    {
        exit(_errmsg(errno, "can't get system sector buffer (%d bytes)\n", dsk_sctsiz));
    }

    if (((bitmap_buf = malloc(dsk_sctsiz)) == 0))    /* L00858 */
    {
        exit(_errmsg(errno, "can't get bitmap sector buffer (%d bytes)\n", dsk_sctsiz));
    }

    if (((opt_no_phys_fmt == 0) && ((ilvptr = (char *)malloc(ilvtbl_siz)) == 0)))    /* L0087e */
    {
        exit(_errmsg(errno, "can't get interleave table (%d bytes)\n", ilvtbl_siz));
    }

    if (_ss_opt(write_path, &dsk_opt) == -1)
    {
        exit(_errmsg(errno, "can't write path options\n"));
    }
}

get_path_opts()      /* L008e0 */
{
    register struct _sgr *optbf = &dsk_opt;

    if (((_gs_opt(write_path, &dsk_opt)) == -1))
    {
        exit(_errmsg(errno, "can't read path options\n"));
    }

    dvc_typ = optbf->_sgr_dtype & 0xff;
    dsk_trk_ofst = optbf->_sgr_toffs & 0xff;
    dsk_sect_ofst = optbf->_sgr_soffs & 0xff;
    is_harddrive = ((dvc_typ & 0xff) & TYP_HARD) != 0;


    if (is_harddrive)      /* else L000972 */
    {
        is_removable = ((dvc_typ & 0xff) & TYP_HREMOV) != 0;      /* L0096e */
    }

    is_floppy = ((dvc_typ & (TYP_SIZ3 | TYP_SIZ5 | TYP_SIZ8 | 8)) != 0);
    dsk_sctsiz = (optbf->_sgr_sctsiz ? optbf->_sgr_sctsiz : 256);  /* L00996 */
    can_autosiz = ((optbf->_sgr_cntl & CNTL_AUTOSIZE) != 0);

    if (opt_dnsity == 0)    /* else L009c8 */
    {
        opt_dnsity = ((optbf->_sgr_dense & 1) ? 2 : 1);     /* L009c4 */
    }

    if (can_autosiz)      /* L009c8 else L00a00 */
    {
        if (((totsects = get_totsect(write_path)) == -1))
        {
            exit(_errmsg(errno, "can't get media capacity\n"));
        }

        dsk_spt = dsk_spt_0 = 32;
    }
    else
    {
        if ((opt_tracks == 0))      /* L00a00 */
        {
            opt_tracks = optbf->_sgr_cyls;
        }

        if ((opt_sides == 0) || (is_harddrive))
        {
            opt_sides = optbf->_sgr_sides & 0x3f;      /* L00a18 */
        }

        dsk_spt = optbf->_sgr_spt;
        dsk_spt_0 = optbf->_sgr_spt0;

    }

    if (opt_ilv)      /* L00a32 */
    {
        optbf->_sgr_intlv = opt_ilv;
    }
    else
    {
        opt_ilv = optbf->_sgr_intlv & 0xff;
    }

    dsk_sas = (optbf->_sgr_salloc & 0xff);

    if ((dsk_sas == 0))
    {
        dsk_sas = 1;
    }

    /* Set minimum sector allocation to the seg. alloc from the descriptor */

    min_sas = optbf->_sgr_salloc & 0xff; /* L00a66 */

    if (is_floppy)
    {
        dvc_siz = (dvc_typ & (TYP_SIZ3 | TYP_SIZ5 | TYP_SIZ8 | 8));
    }
    else
    {
        dvc_siz = (((dvc_typ & 0xff) & DNS_DD) ? 2 : 4);
    }

    dsk_trkDns = ((optbf->_sgr_dense & 14) * 2);
}

/* Set up cluster size, density, sectors per track,
   and make sure hard drives are not formatted without verification */

void
set_dsk_format()      /* L00aae */
{
    if ((opt_clustrsiz == 0))
    {
        opt_clustrsiz = 1;
    }

    fmt_dbldns = (((opt_dnsity == 2) ? FMT_DD : 0));    /* L00acc */

    /* If not Double Density... */
    if ((fmt_dbldns == 0))
    {
        if (dsk_spt_0)
        {
            dsk_spt = dsk_spt_0;
        }

        if ((dsk_spt == 0))      /* L00af0 */
        {
            dsk_spt = dsk_spt_0 = 1;
        }
    }

    /* Don't allow unprompted formatting of Hard Disk */

    if (is_harddrive)      /* L00b00 */
    {
        opt_ready = 0;
    }

    is_ddtrk0 = (((u_char)dvc_typ) & TYP_DDTRK0) && fmt_dbldns;
}

void
report_dsk()      /* L00b26 */
{
    register char *dsksiz;
    register struct _sgr *sgrpt = &dsk_opt;

    puts("------------  Format Data  ------------\n");
    puts("Fixed values:");

    if (is_harddrive)
    {

        printf("               Disk type: hard");

        if (is_removable)
        {
            printf(" (removable)");
        }

        printf("\n");
    }
    else
    {
        switch (dvc_siz)
        {
            case TYP_SIZ8:    /* L00b76 */
                dsksiz = "8\"";
                break;
            case TYP_SIZ5:    /* L00b7c */
                dsksiz = "5 1/4\"";
                break;
            case TYP_SIZ3:    /* L00b82 */
                dsksiz = "3 1/2\"";
                break;
            default:           /* L00b88 */
                dsksiz = "unknown";
        }

        printf("    Physical floppy size: %s\n", dsksiz);
    }


    if (      !(is_harddrive)
            && (dsk_trk_ofst == 1)
            && (dsk_sect_ofst == 1)
            && (dsk_sctsiz == 256)
            && (opt_sides == 2)
            && ((dsk_trkDns & 0x1c) == 4)
            && (fmt_dbldns)
            && ((opt_tracks + dsk_trk_ofst) == 80))
    {
        printf( "                          (Universal Format)\n");
    }

    printf ("             Sector size: %d\n", dsk_sctsiz);     /* L00c10 */

    if (can_autosiz)
    {
        printf("           Disk capacity: %d sectors\n", totsects);
        printf("                         (%d bytes)\n", totsects * dsk_sctsiz);
    }
    else
    {
        printf("           Sectors/track: %d\n", dsk_spt);       /* L00c46 */

        if ((is_harddrive == 0))
        {
            printf("     Track zero sect/trk: %d\n", dsk_spt_0);
        }
    }

    printf("           Sector offset: %d\n", sgrpt->_sgr_soffs);  /* L00c6c */
    printf("            Track offset: %d\n", sgrpt->_sgr_toffs);
    printf("              LSN offset: $%06x\n", sgrpt->_sgr_lsnoffs);

    if ( !(can_autosiz) )   /* else L00cc6 */
    {
        printf("Total physical cylinders: %d\n", (sgrpt->_sgr_totcyls ? sgrpt->_sgr_totcyls : sgrpt->_sgr_cyls));
    }

    printf(" Minimum sect allocation: %d\n\n", min_sas);    /* L00cc6 */
    puts("Variables:");

    if ( !(is_harddrive) )      /* else L00dbc */
    {
        printf("        Recording format: %s", (fmt_dbldns ? "MFM" : "FM"));

        if (is_ddtrk0)
        {
            fputs(" all tracks", stdout);
        }

        printf("\n");      /* L00d1a */
        printf("    Track density in TPI: ");

        switch (dvc_siz)
        {
            case 2:         /* L00d30 */
                dsksiz = "48";
                break;
            case 4:         /* L00d54 */
                switch (dsk_trkDns & 0x1c)
                {
                    case DNS_DT<<1:      /* L00d38 */
                        dsksiz = "96";
                        break;
                    case DNS_QT<<1:      /* L00d40 */
                        dsksiz = "192";
                        break;
                    case DNS_OT<<1:     /* L00d48 */
                        dsksiz = "384";
                        break;
                    default:     /* L00d4e */
                        dsksiz = "48";
                        break;
                }

                break;
            case 6:         /* L00d74 */
                dsksiz = ((dsk_trkDns & 0xff) & 4) ? "135" : "62.5";
                break;
            default:        /* L00d8a */
                dsksiz = "unknown";
                break;
        }

        printf("%s\n", dsksiz);
    }

    if ((can_autosiz == 0))    /* L00dbc */
    {
        printf("Number of log. cylinders: %d\n", opt_tracks);
        printf("      Number of surfaces: %d\n", opt_sides);
    }

    printf("Sector interleave offset: %d\n\n", opt_ilv);
}

int
input_str(d4, a2)      /* L00dfc */
    register int d4;
    register char *a2;
{
    if (!(fgets(inkeybuf, sizeof(inkeybuf) - 1, stdin)))
    {
        if (errno)
        {
            prerr(0, errno);
        }

        stdin->_flag &= ~(_EOF);    /* L00e38 */
        inkeybuf[0] = '\0';
        fputs("\n", stdout);
    }
    else
    {
        /* L0e6e */
        while ((--d4 >= 0))
        {
            if ((a2[d4] == (*inkeybuf | 0x20)))
            {
                return (d4 + 1);
            }
        }
    }

    return 0;
}

void
set_floppy_fmt()      /* L00e7e */
{
    register int resp;

    /* Density... */

    if ((D00548 == -1))
    {
        D00548 = dsk_trkDns;
    }
    else
    {
        dsk_trkDns = (short)D00548;
    }

    do
    {
        fputs("Double density? ", stdout);
    } while ((resp = input_str(2, "ny")) == 0);

    opt_dnsity = resp;

    /* Sides... */

    do
    {
        fputs("Double sided? ", stdout);
    } while ((resp = input_str(2, "ny")) == 0);

    opt_sides = resp;

    /* Tracks... */

    fputs("Number of logical tracks (cyls)? ", stdout);
    input_str(0);

    /* This is a kludgy way to get the address of the LSB of tik_tot */

    str_to_digit((int)(&tik_tot) + 3);
    opt_tracks = (short)tmpnum;

    if (dsk_trkDns)
    {
        switch (dvc_siz)
        {
            case TYP_SIZ5:     /* L00f1e */
                adj_flop_tpi();
                break;
            case TYP_SIZ3:     /* L00f22 */
                do
                {
                    fputs("Change to 62.5 TPI for format? ", stdout);
                } while ((resp = input_str(2, "ny")) == 0);

                dsk_trkDns = ((resp == 2) ? 0 : 4);
            case TYP_SIZ8:     /* L00f70 */
                break;
        }
    }
}

adj_flop_tpi()      /* L00f7a */
{
    register int resp;
    register int unchanged = 0;

    if (((dsk_trkDns & 0xff) & (DNS_OT<<1)))   /* else L00fc0 */
    {
        do
        {
            printf("Change to 192 TPI for format? ");
        } while ((resp = input_str(2, "ny")) == 0);

        if ((resp == 2))
        {
            dsk_trkDns = (DNS_QT<<1);
            return 0;
        }

        unchanged = 1;      /* L00fbe */
    }

    if ((unchanged) || ((dsk_trkDns & 0xff) & (DNS_QT<<1)))    /* L00fc0 */
    {
        do
        {
            printf("Change to 96 TPI for format? ");
        } while ((resp = input_str(2, "ny")) == 0);

        if ((resp == 2))
        {
            dsk_trkDns = (DNS_DT<<1);
            return 0;
        }
        else
        {
            unchanged = 1;
        }
    }

    if ((unchanged) || ((dsk_trkDns & 0xff) & (DNS_DT<<1)))    /* L00ff6 */
    {
        do
        {
            printf("Change to 48 TPI for format? ");
        } while ((resp = input_str(2, "ny")) == 0);

        if ((resp == 2))
        {
            dsk_trkDns = 0;
            return 0;
        }

        /* These following returns give warnings, but makes the code match */
        return;

    }
    else
    {
        return;
    }

    return 0;
}
