#include <stdio.h>
#include <ctype.h>
#include <setsys.h>
#include "fmt.h"


void showhelp();    /* L02a5c */
char *str_to_digit();
char *dnsorsid();
void opt_err();
void fput_line();
void int_to_short();
void int_to_byt3();

get_opts(cur_opt)
    register char *cur_opt;
{
    int v0;

    while (*(++cur_opt))
    {
        switch (_tolower(*cur_opt))
        {
            case '?':       /* L028a6 */
                showhelp();
                exit(0);
            case 'c':       /* L028b0 */
                cur_opt = str_to_digit(cur_opt);
                opt_clustrsiz = (short)tmpnum;

                if ((tmpnum == 0) || ((tmpnum -1) & tmpnum))
                {
                    exit(_errmsg(1, "cluster size must be a power of 2\n"));
                }

                break;
            case 'd':       /* L028dc */
                cur_opt = dnsorsid(cur_opt, 2);
                continue;
            case 'e':       /* L028e2 */
                opt_showvfytime = 1;

                if (((Tck_Sec = _getsys(D_TckSec, 2)) == -1))
                {
                    exit (_errmsg(errno, "can't get system ticks/sec\n"));
                }

                break;
            case 'i':       /* L02912 */
                cur_opt = str_to_digit(cur_opt);
                opt_ilv = (short)tmpnum;
                break;
            case 'n':       /* L0294e */
                switch ((_tolower(*(++cur_opt))))
                {
                    case 'p':    /* L02924 */
                        opt_no_phys_fmt = 1;
                        break;
                    case 'v':    /* L0292e */
                        opt_no_phys_vfy = 1;
                        break;
                    case 'f':    /* L02938 */
                        opt_no_fast_vfy = 1;
                        break;
                    default:
                        opt_err(&(cur_opt[-1]));
                        break;
                }

                break;
            case 'r':       /* L02972 */
                opt_ready = 1;
                break;
            case 's':       /* L0297c */
                cur_opt = dnsorsid(cur_opt, 1);
                continue;
            case 't':       /* L0298a */
                cur_opt = str_to_digit(cur_opt);
                opt_tracks = (short)tmpnum;
                break;
            case 'v':       /* L0299c */
                if (*(++cur_opt) == '=')
                {
                    ++cur_opt;
                }

                opt_VolName = cur_opt;
                if ((v0 =  strlen(cur_opt)) > 32)
                {
                    exit(_errmsg(1, "volume name to long (32 characters maximum)\n"));
                }
                else {
                    cur_opt += v0 - 1;
                }

                break;
            default:        /* L029d6 */
                opt_err(cur_opt);
        }
    }
}

char *
hlpmsg[] = {
    "Syntax: format [<opts>] <devname> [<opts>]\n",
    "Function: format disk media\n",
    "Options:\n",
    "    -c=<num>   cluster size (1)\n",
    "    -dd        double density (floppy) disk\n",
    "    -ds        double sided (floppy) disk\n",
    "    -e         display elapsed verify time\n",
    "    -i=<num>   interleave offset value\n",
    "    -np        inhibit physical format\n",
    "    -nv        inhibit physical verify\n",
    "    -nf        inhibit fast mode verify\n",
    "    -r         ready (don't ask)\n",
    "    -sd        single density (floppy) disk\n",
    "    -ss        single sided (floppy) disk\n",
    "    -t=<num>   number of cylinders (tracks)\n" ,
    "    -v=<name>  volume name\n" 
};

/*This seems to be a rather useless variable, as it could be defined inline just as easily */

char **hlp_end = &(hlpmsg[sizeof(hlpmsg)/sizeof(hlpmsg[0])]);

void
showhelp()/* L02a5c */
{
    register char **curline = hlpmsg;

    while (curline < hlp_end)
    {
        fput_line(*(curline++));
    }
}

char*
str_to_digit(optptr)/* L02a8a() */
    register char *optptr;
{
    if ((*(++optptr)) == '=')
    {
        ++optptr;
    }

    tmpnum = atoi(optptr);

    do {
    } while (isdigit(*(optptr++)));

    return &(optptr[-2]);
}

char*
dnsorsid(optptr,value)   /* L02ad6 */
    register char *optptr;
    register int   value;
{
    switch (_tolower(*(++optptr)))
    {
        case 'd':
            opt_dnsity = value;
            break;
        case 's':
            opt_sides = value;
            break;
        default:
            opt_err(&(optptr[-2]));
            break;
    }

    return optptr;
}

void
opt_err(badopt)
    register char *badopt;
{
    showhelp();
    exit(_errmsg(1, "unknown option '%s'\n", badopt));
}

void
fput_line(op)/* L02b5c */
    register char *op;
{
    fflush(stdout);
    fputs(op, stderr);
}

/*Convert int to short (defined as a 2-byte character string) */

void
int_to_short(val,dst)      /* L02b90 */
    register short val;
    register char *dst;
{
    dst[1] = val;
    dst[0] = (val >>= 8);
}

/*Convert int to 3-byte value (defined as a 3-byte character string) */

void
int_to_byt3(src,dstval)        /* L02bb8 */
    register int   src;
    register char *dstval;
{
    dstval[2] = src;
    dstval[1] = (src >>= 8);
    dstval[0] = (src >>= 8);
}


/*Returns Drive size in sectors
 *  Passed:  path #
 */

/* NOTE: To get a perfect code match, anytime this module needs to be built,
 * you first need to do "make fot_06.a"
 * Then edit fmt_06.a by doing the following:
 * Go to the dnd of the preceding function (int_to_short() )
 * Delete the last two lines (unlk a5, rts)
 * add " bsr.s L02c02"
 * then proceed with make

 * This is not necessary if you are not trying to match original code
 
 */

#asm
get_totsect: link.w A5,#0      
 movem.l a0/d1-d2,-(sp) 
 move.l #64,d1 SS_DSize in sectors
 os9 I$GetStt   
 bcs.w  _os9err     
 move.l d2,d0      
 movem.l -12(a5),a0/d1-d2 
L02c02 unlk A5
 rts
#endasm
