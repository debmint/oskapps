#include <stdio.h>
#include <time.h>
#include "dircmd.h"

char spaces[] = "                              ";
char prms_cd[] = "ewrewrewr";
char prms_dsk[] = "dsewrewr";
int hdr_prnted = 0;
int cdprm_msks[] = {0x8000, 0x0400, 0x40, 0x04}; /* D0102e */

void prnt_ext_lin();
void colhdr();

extern struct dir_fd fd_data;
extern short opt_e, opt_fmtted, is_cdfm;
extern int opt_dirslash;
extern struct sgtbuf nowtime;
extern short prnt_as_dir;
extern int linpos;

#define REMAINS(s) (80 - (strlen(s) +  22))

/* Output the "Directory of..." text if applicable */

void
dohdr(dname)    /* L00133c */
  char *dname;
{
    int indnt;
    int v0;

    if (opt_fmtted)
    {
        getime(&nowtime);

        indnt = 30 - (REMAINS(dname)/2);

        if (indnt > 30)
        {
            indnt = 30;
        }

        printf ("%sDirectory of %s %02d:%02d:%02d\n",
            &spaces[indnt], dname, nowtime.t_hour, nowtime.t_minute, nowtime.t_second
        );

        if (opt_e)
        {
            colhdr();
        }
    }
}

char *
basename(nam)    /* L013de */
  register char *nam;
{
    register char *tmpnam = nam;  /* A3 */

    while (*(nam++))
    {
        if ((*nam == '/'))
        {
            tmpnam = &(nam[1]);
        }
    }

    return tmpnam;
}

/* Output a single entry.
 * If -e is specified, it outputs a formatted line
 * othewise, it adds the name as appropriate
 *
 * Passed:
 *   pthname: The filenae (possibly full pathlist)
 *     isdir: True if it's a directory
 */

void
prnt_name(pthnam, is_dir)    /* L01414 */
  register char *pthnam;      /*  a2  */
int is_dir;
{
    if (opt_e)
    {
        prnt_ext_lin(pthnam, is_dir);
        return;
    }

    if (opt_fmtted)       /* L0143c */
    {
        pthnam = basename(pthnam);

        if (linpos > 65)
        {
            putc('\n', stdout);
            linpos = 1;
        }
    }

    fputs(pthnam, stdout);    /* L01462 */
    
    if (is_dir && opt_dirslash)
    {
        putc('/', stdout);
        ++linpos;
    }

    if (!opt_fmtted)
    {
        putc('\n', stdout);
    }
    else
    {
        linpos += strlen(pthnam);    /* L0149e */
        putc(' ', stdout);

        if ((linpos < 65))
        {
            while (linpos++ % 16)
            {
                putc(' ', stdout);
            }
        }
    }
}

/* Print the oolumn header for extended listing
 * Passed:
 *   pthnam: The path to the entry (possibly a full pathlist)
 *   is_dir: True if it/s a directory
 */

void
prnt_ext_lin(pthnam, is_dir)        /* L014e4 */
  char *pthnam;
  int is_dir;
{
    register struct dir_fd *a2 = &fd_data;
    struct sgtbuf *v0;

    if (opt_fmtted)
    {
        pthnam = basename(pthnam);
    }

    v0 = (struct sgtbuf *)a2->date;    /* L0150e */

    /* Output owner, group, and Date Modified */

    printf((is_cdfm ? "%3u.%-3u %02d/%02d/%02d %02d%02d:%02d "
                    : "%3u.%-3u   %02d/%02d/%02d %02d%02d   "),
            (short)(a2->owners.own_split.owner & 0xff),
            (short)(a2->owners.own_split.group & 0xff),

            /* Here is where the year needs to be fixed for y2k */
            v0->t_year + 1900, v0->t_month, v0->t_day,
            v0->t_hour, v0->t_minute, v0->t_second
    );

    /* Output the permissions - note that perms for disk and CD differ */

    if (is_cdfm)
    {
        /* A kludge here -- Had to cast a2->attrib to get code to match */
        do_cprms_dsk((short)(a2->attrib));
    }
    else
    {
        /* A kludge here -- Had to cast a2->attrib to get code to match */
        do_dskperms((short)(a2->attrib));    /* L01594 */
        putc(' ', stdout);
    }

    /* Print the remainder of the line */

    printf(" %6lX %9ld %-s", a2->fd_seg, a2->size, pthnam);   /* L015aa */

    if (is_dir && opt_dirslash)
    {
        putc('/', stdout);
    }

    putc('\n', stdout);
}

void
colhdr()
{
    if (!(hdr_prnted) || prnt_as_dir)
    {
        puts(" Owner     Last modified   Attributes Sector Bytecount Name");
        puts("-------   ---------------  ---------- ------ --------- ----");
        hdr_prnted = 1;
    }
}

/* Set up the permissions string to match those of the file/directory
 * - disk version
 */

do_dskperms(perm)    /*L01634 */
  int perm;
{
    register char *fld = prms_dsk;
    register int msk = 128;

    while (*fld)
    {
        putc(((perm & msk) ? *fld : '-'), stdout);
        ++fld;
        msk >>= 1;
    }     /* Test @ L01670 */
}

/* Set up the permissions string to match those of the file/directory
 * - CD version
 */

do_cprms_dsk(perm)    /* L0167e */
  register int perm;
{
    register char *prm_ltr = prms_cd;
    register int  *masks = cdprm_msks;
    register int   msk = *(masks++);

    putc (((perm & msk) ? 'd' : '-'), stdout);    /* putc @ L016ae */

    while (*prm_ltr)
    {
        register int msm_pos;

        msk = *(masks++);    /* L016b4 */
        msm_pos = 0;

        while (msm_pos < 3)
        {
            putc(((perm & msk) ? *prm_ltr : '-'), stdout);
            ++prm_ltr;
            msk >>= 1;
            ++msm_pos;
        }
    }
}
