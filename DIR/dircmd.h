#include <types.h>
 
/* a directory entry */
 
typedef struct dir2list
{
    struct dir2list *prevdir;   /* 00 */
    struct dir2list *nxtdir;    /* 04 */
    struct dir2list *de_up;     /* 08 */
    u_int  fdsect;              /* 12 */
    u_int  isdir;               /* 16 */
    char name[1];               /* 20 */   /* Length varies */
} DIR_ENT;

struct dir_fd {
    union {
        u_long grp_own;
        struct {
            u_short owner,
                    group
        } own_split;
    } owners;
    u_int fd_seg;
    long  size;        /*  8 */
    u_short attrib;    /* 12 */
    short isDir;       /* 14 */
    char  date[6];     /* 16 */
};

/* This is some struct that needs to be defined */
struct subdirs
{
    char           *s_name;
    struct subdirs *nxt_sbdr;
}; 

/* The first 16 bytes of a file descriptor- all we use */

struct fdesc_hd
{
    char	fd_att,			/* file attributes */
		fd_own[2],		/* file owner */
		fd_date[5],		/* date last modified */
		fd_link,		/* link count */
		fd_fsize[4],	/* file size */
		fd_dcr[3];		/* date created */
};

