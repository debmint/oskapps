
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <modes.h>
#include <sgstat.h>
#include <time.h>
#include <rbf.h>
#include <direct.h>
#include <sg_codes.h>
#include <cdi.h>
#include "dircmd.h"

#define SEEK_SET 0

struct dir_fd   fd_data;
DIR_ENT        *dirents_base;       /* D00034 */
struct subdirs *dirs_begin;   /* D00038 */
struct _sgr     optstbl;      /* sgbuf for RBF device */ /* D0003c */
struct sgtbuf   nowtime;
char            curnt_nam[256];
char            full_path[256];
u_int           fd_loc;
int    linpos;
short  opt_e;         /* D002ca */
int    opt_recurse;   /* D002cc */
int    opt_all;       /* D002d0 */
int    opt_dirasfile; /* D002d4 */
int    opt_x;         /* D002d8 */
int    opt_dirslash;  /* D002dc */
short  opt_fmtted;    /* D002e0 */
short  opt_sorted;    /* D002e2 */
short  prnt_as_dir;   /* D002e4 */
short  is_cdfm;       /* D002e6 */
short  opt_z;         /* D002e8 */
char  *zfile;         /* D002ea */
int    usrID;         /* D002EE */
union {
    struct direntry dsk[64];
    _cdifd          cd
}  DirEnts;         /* D002f2 */

int D00af2;         /* D00af2 */
int cd_fsiz;        /* D00af6 */
int need_cdfdinfo;         /* D00afa */
_cdifd *cdfd_bgn;  /* D00afe */

struct fdesc_hd fd_inf;
FILE  *zfile_fp;
char            znamebuf[256];                      /* D00b16 */
struct subdirs **last_dir = &dirs_begin;             /* D00f9e */
char            dir_dot[] = ".";                    /* D00fa2 */
char           *cantopen = "can't open \"%s\". ";   /* D00fa4 */
char            FDreadErr[] = "can't read FD sector. ";

char *name_from_zfile();    /* L004c0 */
void *get_mem();
DIR_ENT *de_first();
void do_toplevel();
void output_entry();
void do_subdirs();
void no_dirname();
void dsk_do_dir();
void cd_do_dir();
_fdhead *get_cd_dirent();
void output_dir_struc();
u_int get_fdseg();
void prnt_sorted();
void do_recursion();
DIR_ENT *nxt_dirent();
char *cpy_nam();

extern void dohdr();
extern int prnt_name();
extern void colhdr();
extern void add_dirent();

int
get_opts(optpt)
  register char *optpt;
{
    while (*(++optpt))
    {
        switch (_tolower(*optpt))
        {
            case '?':   /* L02a4 */
                showhelp();
                exit(0);
            case 'z':   /* L02ae */
                opt_z = 1;
                ++optpt;

                if (*optpt == '=')
                {
                    ++optpt;
                    zfile = optpt;
                    return 0;
                }

                --optpt;
                continue;
            case 'x':   /* L02c8 */
                opt_x = 1;
                continue;
            case 'e':   /* L02d2 */
                opt_e = 1;
                break;
            case 'n':   /* L02dc */
                opt_dirasfile = 1;
                continue;
            case 'u':   /* L02e6 */
                opt_fmtted = 0;
                continue;
            case 's':   /* L02ee */
                opt_sorted = 0;
                break;
            case 'a':   /* L02f6 */
                opt_all = 1;
                continue;
            case 'd':   /* L0300 */
                opt_dirslash = 1;
                continue;
            case 'r':   /* L030a */
                opt_recurse = 1000;
                ++optpt;

                if (*optpt == '\0')
                {
                    goto ret000;
                }

                if (*optpt == '=')
                {
                    ++optpt;
                }

                if (isdigit(*optpt))
                {
                    if ((opt_recurse = atoi(optpt)) <= 0)
                    {
                        exit(_errmsg(1, "illegal size for -r option\n"));
                    }

                    goto ret000;
                }

                --optpt;
                continue;
            default:
                showhelp();
                exit(_errmsg(1, "unknown option '%c'\n", *optpt));
                break;
        }
    }

ret000:    return 0;
}

void
main (argc, argv)
  register int argc;
  register char **argv;
{
    register int dircount = 0;
    register char *argptr;
    register DIR_ENT *d_ent;

    opt_sorted = opt_fmtted = 1;
    linpos = 1;
    usrID = getuid();

    while (--argc)
    {
        argptr = *(++argv);

        if (*argptr == '-')
        {
            get_opts(argptr);
            *argv = '\0';
        }
        else   /* It's a filename */
        {
            ++dircount;
            add_dirent(*argv);
        }
    }    /* while test @ L0456 */

    d_ent = dirents_base;

    /* if no dir or file is specified ... */

    if ((dircount == 0) && !(opt_z))   /* else L046e */
    {
        no_dirname();  /* go to L0049e */
    }
    else
    {          /* L0046e */
        if (dircount)    /* else L00496 */
        {
            cmdline_dirs(d_ent);
            do_subdirs();
        }
        
        /* Now process any z-names */

        while (argptr = name_from_zfile())    /* Test @ L00492, L00496 */
        {
            /* Start dirs-list clean on each iteration */
            dirs_begin = 0;
            last_dir = &dirs_begin;
            do_toplevel(argptr);
    
            if (prnt_as_dir)
            {
                do_subdirs();
            }
        }
    }

    if (opt_fmtted && !opt_e)
    {
        putc('\n', stdout);
    }
}

/* Retrieve names of dirs or files to list spe\cified in
 * the -z opt, if provided.  This func determines whether
 * input s from a file or stdin.  If a file, it ist opens the file,
 * then on successive calls, returns the names on by one.
 */

char *
name_from_zfile()
{
    register char *a2;

    if (opt_z)
    {
        if (zfile_fp == 0)   /* else L0051a */
        {
            if (zfile)   /* else L00512 */
            {
                if (!(zfile_fp = fopen(zfile, "r")))
                {
                    exit(_errmsg(errno, cantopen, zfile));
                }
            }
            else   /* L00512  */
            {
                zfile_fp = stdin;
            }
        }

        do
        {
        } while ((a2 = fgets(znamebuf, 256, zfile_fp)) && (*znamebuf == '*'));

        if (a2 && (*znamebuf != '\n'))
        {
            znamebuf[strlen(znamebuf) - 1] = '\0';
            return znamebuf;
        }
    }

    return 0;
}

/* Process all names specified on the command line */

cmdline_dirs(de)    /* L00572 */
  register DIR_ENT *de;
{
    if (de)
    {
        de = de_first(de);

        do
        {
            do_toplevel(de->name);
        } while (de = nxt_dirent(de));
    }
}

void *
get_mem(mreq)
  unsigned int mreq;
{
    register void *mempt;

    if (!(mempt = malloc(mreq)))
    {
        exit(_errmsg(errno, "can't get memory. "));
    }

    return mempt;
}

void
free_mem(curnt_d)
register DIR_ENT *curnt_d;
{
    register DIR_ENT *nxt_d;

    if (curnt_d)
    {
        while (curnt_d)
        {
            if (curnt_d->prevdir)    /* L00606 */
            {
                curnt_d = curnt_d->prevdir;
                continue;
            }
            else if (curnt_d->nxtdir)    /* L0060e */
            {
                curnt_d = curnt_d->nxtdir;
                continue;
            }
            else if ((nxt_d = curnt_d->de_up))    /* L0061a */
            {
                if (nxt_d->prevdir == curnt_d)
                    nxt_d->prevdir = 0;
                else
                    nxt_d->nxtdir = 0;    /* L0062a */
            }

            free (curnt_d);    /* L0062e */
            curnt_d = nxt_d;
        }
    }
}

/* Move DIR_ENT pointer to first element of (tree) list */

DIR_ENT *
de_first(ch)    /* L00644 */
  register DIR_ENT *ch;
{
    for ( ; ; )
    {
        if ((ch->prevdir == 0))
            break;
        ch = ch->prevdir;
    }

    return ch;
}

/* Process a top-level file or directory name in a commandline or z-list */

void
do_toplevel(fname)    /* L0066c */
  char *fname;
{
    register struct subdirs *newstr;
    register int pathnum;

    if (_parsepath(fname) == -1)
    {
        exit(_errmsg(errno, cantopen, fname));
    }

    pathnum = fil_opn(fname);

    if (prnt_as_dir)         /* $006a6 ELSE L006ca */
    {
        *last_dir = newstr = (struct subdirs *)get_mem(sizeof(struct subdirs)); /* 006b4 */
        last_dir = &(newstr->nxt_sbdr);
        newstr->nxt_sbdr = 0;
        newstr->s_name = fname;   /* L006c6 */
    }
    else
    {
        output_entry(fname, pathnum, prnt_as_dir);
    }

    close(pathnum);
}

void
do_subdirs()        /* L006ec */
{
    register int rcrs = opt_recurse;
    register struct subdirs *_dir = dirs_begin;
    register int pthnum;

    if (_dir)
    {
        do
        {
            linpos = 1;

            if (opt_fmtted)
            {
                putc('\n', stdout);
            }

            pthnum = dir_opn(_dir->s_name);
            output_dir_struc(_dir->s_name, rcrs, pthnum);
            close(pthnum);
        } while ((_dir = _dir->nxt_sbdr));
    }
}

/* Call this function if no name is specified on the command line
 * I.E. "dir ."
 */

void
no_dirname()    /* L0074c */
{
    register int _path;

    output_dir_struc(dir_dot, opt_recurse, (_path = dir_opn(dir_dot)));
    close(_path);
}

/* Output all the files/dirs in the given dir.
 * This can be:
 *  1. ".", if no name is specified on the command line
 *  2. one of the names specified on the command line
 *  3. one of the names specified in the z-file or std input
 *
 * If recursion is specified
 * this function is recursively called for each directory structure
 */

void
output_dir_struc(fname, rcrs, fp)    /* L0078a */
  register char *fname;
  register int rcrs;
  register int fp;
{
    register DIR_ENT *dE;

    dirents_base = 0;
    dohdr(fname);
    _gs_opt(fp, &optstbl);

    /* Output the data for the current directory or file
     * Note that the retrieval of info is different for
     * a disk and a CD
     */

    if (is_cdfm = (optstbl._sgr_class == DT_CDFM))
    {
        cd_do_dir(fname, rcrs, fp);
    }
    else
    {
        dsk_do_dir(fname, rcrs, fp);
    }

    dE = dirents_base;

    /* Current directory has been printed, now recursively process
     * subdirs if recursion is in effect
     */

    if (rcrs)
    {
        do_recursion(dE, rcrs);
    }

    free_mem(dE);
}

/* Called from output_dir_struc()
 * Parses past "." and ".." and then reads in the dirents one by one
 * and outputs them.
 * If output is to be sorted the names are added to a DIR_ENT treelist
 * and listed at end when all names are retrieved
 */

void
dsk_do_dir(dirPth, rcrs, fp)    /* L00804 */
  register char *dirPth;
  register int rcrs;
  register int fp;
{
   register struct direntry *_dirent = DirEnts.dsk;
   int numrecs;

    /* Try to seek past "." and ".." */

    if ((lseek(fp, (2 * sizeof(struct direntry)), SEEK_SET) == -1))
    {
        exit(_errmsg(errno, "can't seek past \".\" and \"..\". "));
    }

    /* Get dirents from file struct and process one by one */

    while (((numrecs = read(fp, DirEnts.dsk, sizeof(struct direntry)))) > 0)
    {
        register short is_dir;

        if (((_dirent->dir_name)[0] != '\0') && (((_dirent->dir_name)[0] != '.') || opt_all))      /* L0084a */
        {
            fd_loc = _dirent->dir_addr;
            cpy_nam(curnt_nam, _dirent->dir_name);   /* L00862 */

            if (dirPth != dir_dot)   /* path specified */
            {
                strcpy(full_path, dirPth);
                strcat(full_path, "/");
                strcat(full_path, curnt_nam);
            }
            else    /* Simply "dir" or "dir ." */
            {
                strcpy(full_path, curnt_nam);
            }

            if (rcrs || (!opt_sorted && (opt_e || opt_dirslash)))
            {
                is_dir = get_fdseg(fp, fd_loc);   /*  L008d0 */
            }

            if (!opt_sorted)     /* L008dc */
            {
                prnt_name(full_path, (int)is_dir);
            }

            /* If the output is supposed to be sorted, just add the
             * entry to the sorted list but don't print yet
             */

            if (opt_sorted || (rcrs && is_dir))
            {
                add_dirent(full_path);
            }
        }
    }

    if (numrecs == -1)
    {
        exit(_errmsg(errno, "directory read error. "));
    }

    /* We now print the sorted list */

    if (opt_sorted)
    {
        prnt_sorted(dirents_base, fp);
    }
}

/* Called from do_toplevel
 */

void
output_entry(filnam, pathnum, p3)    /* L0095a */
  register char *filnam;
  register int   pathnum;
  int p3;
{
    char bfr[76];  /* until we can determine what it is */

    if (opt_e)    /* else L00a44 */
    {
        if ((getstat(SS_Opt, pathnum, &optstbl) == -1))
        {
            exit(_errmsg(errno, "can't get sector address. "));
        }

        is_cdfm = (optstbl._sgr_class == DT_CDFM);   /* L009a2 - L009b0 */

        if (is_cdfm)   /* else L009ee */
        {
            if ((get_cdfm(pathnum, bfr, sizeof(bfr))) == -1) /* else L009e4 */
            {
                exit(_errmsg(errno, FDreadErr));
            }

            cdfd2std_fd(bfr);    /* L009e4 */
        }
        else
        {        /* L009ee */
            register int sctsz;


            sctsz = (!optstbl._sgr_sctsiz ? 256 : optstbl._sgr_sctsiz);

            if((_gs_gfd(pathnum, &fd_inf, sizeof(fd_inf)) == -1))
            {
                exit(_errmsg(errno, FDreadErr));
            }

            xlate_fdinf((optstbl._sgr_fdpsn)/sctsz);
        }

        if (opt_fmtted)    /* L00a3a */
        {
            colhdr();
        }
    }

    prnt_name(filnam, p3);    /* L00a44 */
}

/* Handle CDFM dirs */

void
cd_do_dir(fname, rcrs, fp)    /* L00a5c */
  char *fname;
  register int rcrs;
  register int fp;
{
    register _fdhead *fdhd;
    cdfd_bgn = &DirEnts.cd;
    D00af2 = 0;
    need_cdfdinfo = 1;
    fdhd = get_cd_dirent(fp);
    cd_fsiz = fdhd->_fd_filesize;
    get_cd_dirent(fp);

    while ((fdhd = get_cd_dirent(fp)))
    {
        if ((*fdhd->_fd_name != '.') || opt_all)    /* L00a9e */
        {
            register int nmlen = fdhd->_fd_namelen;    /* namelen */
            register int zilcho;    /* Not used - just to make push match */
            register char *dummy;   /* another var to match push */

            strncpy(curnt_nam, fdhd->_fd_name, nmlen);
            curnt_nam[nmlen] = '\0';    /* 0x000x00aca */

            if (dir_dot != fname)        /* ELSE L00b08 */
            {
                strcpy(full_path, fname);
                strcat(full_path, "/");
                strcat(full_path, curnt_nam);
            }
            else
            {
                strcpy(full_path, curnt_nam);    /* L0b08 */
            }

            cdfd2std_fd(fdhd);      /* L00b18 */
            prnt_name(full_path, fd_data.isDir);

            if ((rcrs) && (fd_data.isDir))
            {
                add_dirent(full_path);
            }
        }
    }    /* L00b42 */
}

_fdhead *
get_cd_dirent(fp)    /* L00b58 */
  register int fp;
{
    register _cdifd *a2;

    if ((D00af2 != 0))
    {
        if ((D00af2 >= cd_fsiz))
            return 0;
    }

    if (need_cdfdinfo)       /* else L00bb2 */
    {
        if ((read(fp, &(DirEnts.cd), sizeof(DirEnts)) <= 0))   /* else L00b9c */
        {
            return 0;
        }

        D00af2 = ceil_2048(D00af2);    /* L00b9c */
        cdfd_bgn = &DirEnts.cd;
        need_cdfdinfo = 0;
    }

    a2 = cdfd_bgn;            /* L00bb2 */
    D00af2 += (char)(cdfd_bgn->head._fd_rlen);
    /*nextfd(cdfd_bgn);*/
    /* The preceding line is probably the correct one, but to get the code
     * to match, the following line had to be used */ 
    (int)cdfd_bgn += (char)(cdfd_bgn->head._fd_rlen);

    /* The following is a hacked-up mess, but it's the best I could
     * do to get the code to match
     */

    if ((((u_int)cdfd_bgn) >= (u_int)(&(((char *)DirEnts.dsk)[sizeof(DirEnts.dsk) - 1])) ) || (cdfd_bgn->head._fd_rlen == 0))
    {
        need_cdfdinfo = 1;
    }

    return a2;
}

/* I could not get the c code to match, although it was functionally
 * identical.  At least for debugging purpose, we'll do a straight
 * assembly
 */
/* The original code*/

#asm
ceil_2048: link.w A5,#0
 movem.l d0-d1/d4,-(sp)
 move.l d0,d4
 move.l #-64,d0
 bsr.w _stkchec
 move.l d4,d0
 move.l #2048,d1
 bsr.w _T$LMod
L00c16 beq.s L00c32
 move.l d4,d0
 bge.s L00c22
 addi.l #2047,d0
L00c22 moveq #11,d1
 asr.l d1,d0
 moveq #11,d1
 lsl.l d1,d0
 addi.l #2048,d0
 move.l d0,d4
L00c32 move.l d4,d0
 movem.l -8(a5),d1/d4
 unlk A5
 rts 
#endasm

/*ceil_2048(p1)*/        /* L00bf6 */
/*register int p1;
{
    if (((p1 % 2048)))
    {
        p1 = (((p1 < 0 ? 2047 + p1 : p1) >> 11) << 11) + 2048;
    }

    return p1;
}*/

int
fil_opn(path)        /* L0c3e */
register char *path;
{
    register int mode = opt_x ? S_IEXEC : 0;
    register int p_num;

    if (((p_num = open(path, mode)) == -1))
    {
        return dir_opn(path);
    }
    else
    {
         prnt_as_dir = 0;
         return p_num;
    }
}

int
dir_opn(pathname)
register char *pathname;
{
    register int pthnum;
    register int o_mode = opt_x ? S_IEXEC : 0;    /* L00ca6 */

    if (((pthnum = open(pathname, (S_IFDIR | S_IREAD) | o_mode)) == -1))
    {
        exit(_errmsg(errno, "can't open \"%s\". ", pathname));
    }

    prnt_as_dir = (opt_dirasfile ? 0 : 1);
    return pthnum;
}

cdfd2std_fd(fdhd)     /* L00cf2 */
register _fdhead *fdhd;
{
    register _fdtail *fdtailpt;
    register struct dir_fd *std_fd = &fd_data;
    register u_int d4 = fdhd->_fd_namelen;

    fdtailpt = (_fdtail *)(&(fdhd->_fd_name)[d4] + ((d4 & 1) ? 0 : 1));
    std_fd->isDir = fdtailpt->_efd_attr & -32768;
    std_fd->fd_seg = fdhd->_fd_fileaddr;
    std_fd->size = fdhd->_fd_filesize;
    _strass(std_fd->date, fdhd->_fd_datetime, sizeof(std_fd->date));
    std_fd->owners.grp_own = fdtailpt->_efd_owner;
    std_fd->attrib = fdtailpt->_efd_attr;
}

/* Load file descriptor for currently open entity and move data to
 * fd_inf translated into more usable forms.
 */

u_int
get_fdseg(fp, fd_seg)    /* L00d68 */
int fp;
u_int fd_seg;
{
    if (usrID && (setuid(0) < 0))
    {
        exit(_errmsg(errno, "can't change uid to super user\n"));
    }

    load_fdinf(fp, fd_seg, &fd_inf);

    if (usrID)
    {
        setuid(usrID);
    }

    return xlate_fdinf(fd_seg);
}

/* Load first 16 bytes of file's file descriptor */

#asm
load_fdinf:
 movem.l a0/d0-d3,-(sp)
 move.l d1,d3       Sector address to copy
 move.l #32,d1      SS_FDInf (get FD info for specified FD sector)
 move.l #16,d2      Copy first 16 bytes
 movea.l 24(sp),a0  Destination
 os9 I$GetStt
 movem.l (sp)+,a0/d0-d3
 rts
#endasm

/* Move file descriptor data into fd_data translatd into more usable
 * forms
 */

int
xlate_fdinf(fdseg)    /* L00dea */
  u_int fdseg;
{
    register struct dir_fd *dst = &fd_data;
    register struct fdesc_hd *fdpt = &fd_inf;

    dst->fd_seg = fdseg;
    _strass(&(dst->size), fdpt->fd_fsize, 4);
    _strass(dst->date, fdpt->fd_date, 5);
    dst->owners.own_split.owner = fdpt->fd_own[0];
    dst->owners.own_split.group = fdpt->fd_own[1];
    dst->attrib = fdpt->fd_att;
    return (int)(dst->isDir = (fdpt->fd_att & 128));
}

void
prnt_sorted(dir2lst, filpt)    /* L00e62 */
  register DIR_ENT *dir2lst;
  register int filpt;
{
    register struct dir_fd *fildsc = &fd_data;

    if (dir2lst)
    {
        dir2lst = de_first(dir2lst);    /* Walk to begin of list */
    
        do
        {
            if (opt_e || opt_recurse || opt_dirslash)    /* L00e88 */
            {
                dir2lst->isdir = get_fdseg(filpt, dir2lst->fdsect);
            }
    
            prnt_name(dir2lst->name, dir2lst->isdir);
        } while (dir2lst = nxt_dirent(dir2lst));
    }
}
/* Handle recursion.  This function is recursively called by
 * output_dir_struc(), and this function calls output_dir_struc()
 * with a decremented recrse level.  This loop is continued until the
 * subdirs list is depleted or the recursion level reaches 0
 */

void
do_recursion(de, rcrs)    /* L00eca */
  register DIR_ENT *de;
  register int rcrs;
{
    if ((de) && (rcrs > 0))
    {
        de = de_first(de);

        do
        {
            if (de->isdir || !opt_sorted || is_cdfm)    /* L00ef0 */
            {
                register int dir_pth;
                linpos = 1;

                if (opt_fmtted)
                {
                    putc('\n', stdout);
                }

                dir_pth = dir_opn(de->name);    /* L00f1a */
                output_dir_struc(de->name,
                    rcrs - 1,  /* Decrement recursion on each pass */
                    dir_pth);
                close(dir_pth);
            }
        } while ((de = nxt_dirent(de)));    /* L00f3c */
    }
}

DIR_ENT *
nxt_dirent(d2l)        /* L00f50 */
  register DIR_ENT *d2l;
{
    register DIR_ENT *a3;

    /* If there's a "next" (later) entry, go to it and walk back to
     * the earliest entry in that chain */

    if (d2l->nxtdir)
    {
        d2l = d2l->nxtdir;

        while (d2l->prevdir)
        {
            d2l = d2l->prevdir;    /* L00f70 */
        }
        
        return d2l;
    }

    /* If no "next", walk upwards till we find the provided opt_ent */

    while (a3 = d2l->de_up)        /* test @ L00f80 */
    {
        if (a3->prevdir == d2l)    /* L00f7a */
        {
            return a3;
        }

        d2l = a3;
    }

    return a3;
}

char *
cpy_nam(dest, src)    /* L00f94 */
  register char *dest;
  register char *src;
{
    register char *dstptr = dest;

    while (((*(dstptr++) = *(src++)) > 0)) {}

    if (dstptr[-1] != 0)
    {
        dstptr[-1] &= 0x7f;
        *dstptr = '\0';
    }

    return dest;
}

char *hlpmsg[] = {    /* D00fc0 */
    {"Syntax: dir [<opts>] {<dir names> [<opts>]}\n"},
    {"Function: display directory contents\n"},
    {"Options:\n"},
    {"     -a        show all files\n"},
    {"     -d        show directories with a slash\n"},
    {"     -e        extended dir listing\n"},
    {"     -n        treat dirs like files\n"},
    {"     -r        recursive dir listings \n"},
    {"     -r=<num>  recursive dir listing to depth <num>\n"},
    {"     -s        unsorted dir listing\n"},
    {"     -u        unformatted listing \n"},
    {"     -x        directory is execution dir\n"},
    {"     -z        get list of dir names from standard input\n"},
    {"     -z=<path> get list of dir names from <path>\n"}
};

/* showhelp() @ L00fcc */
showhelp()
{
    register char **hlpptr = hlpmsg;
    register char **hlpend = &(hlpmsg[sizeof(hlpmsg)/sizeof(hlpmsg[0])]);

    while (hlpptr < hlpend)
    {
        fputs(*(hlpptr++), stderr);
    }
}

/* This could be made static after debuging */
#asm
get_cdfm:  movem.l a0/d1-d2,-(sp)
 movea.l d1,a0     buffer address
 move.w  18(sp),d2
 move.w  #144,d1    SS_CDFD (CDFM return file descriptor info
 os9     I$GetStt
 bcs.s   L0101e
 moveq   #0,d0
L01018   movem.l (sp)+,a0/d1-d2
 rts
L0101e   moveq  #-1,d0
 move.l  d1,errno(a6)
 ori.b   #1,ccr
 bra.s   L01018
#endasm
/*get_cdfm(d4, bfr, count)
int d4, bfr, count;*/
