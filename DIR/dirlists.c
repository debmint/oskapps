#include <strings.h>
#include "dircmd.h"

extern DIR_ENT *dirents_base;
extern u_int fd_loc;

/* Create a new DIR_ENT
 * Return status:
 *    name and fdsect are filled
 *    all 0ther fields set to 0
 */

DIR_ENT *
creatD2L(nam)    /* L017e2 */
  register char *nam;
{
    register DIR_ENT *new_ent;

    new_ent = (DIR_ENT *)get_mem(sizeof(struct dir2list) - sizeof(new_ent->name) + strlen(nam));
    strcpy(new_ent->name, nam);
    new_ent->fdsect = fd_loc;
    (DIR_ENT *)new_ent->isdir = new_ent->de_up = new_ent->prevdir = new_ent->nxtdir = (DIR_ENT *)0;
    return new_ent;
}

/* Add a new DIR_ENT to the list
 * 1) Create new DIR_ENT via createD2L()
 * 2) If this is first entry, it sets dirents_base to this entry
 * 3) othewise, it places the the entry in the proper sort position in the tree
 */

void
add_dirent(dirname)    /* L01832 */
  register char *dirname;
{
    register DIR_ENT *de_ptr;
    register DIR_ENT *new_entry = creatD2L(dirname);

    if ((de_ptr = dirents_base))     /* else L01888 */
    {
        while (1)
        {
            if ((strcmp(de_ptr->name, new_entry->name) >= 0)) /* L01854 */
            {
                if (de_ptr->prevdir)    /* else L0186e */
                {
                    de_ptr = de_ptr->prevdir;
                    continue;
                }

                de_ptr->prevdir = new_entry;   /* L0186e */
            }
            else
            {
                if (de_ptr->nxtdir)    /* L01872 */
                {
                    de_ptr = de_ptr->nxtdir;
                    continue;
                }

                de_ptr->nxtdir = new_entry;    /* L0187e */
            }

            new_entry->de_up = de_ptr;
            return;
        }
    }
    else
    {
        dirents_base = new_entry;    /* L01888 */
    }
}
